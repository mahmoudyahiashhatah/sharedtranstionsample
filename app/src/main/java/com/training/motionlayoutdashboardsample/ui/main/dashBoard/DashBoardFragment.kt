package com.training.motionlayoutdashboardsample.ui.main.dashBoard

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.training.motionlayoutdashboardsample.R
import kotlinx.android.synthetic.main.fragment_dash_board.*

class DashBoardFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_dash_board, container, false)


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        la_pay.setOnClickListener {
            view.findNavController().navigate(R.id.to_creditListFragment)
        }
        animateStartTransitionTimer()

    }

    private fun animateStartTransitionTimer() {

        val timer = object : CountDownTimer(900, 2) {
            override fun onTick(millisUntilFinished: Long) {}

            override fun onFinish() {
                motionLayout.transitionToEnd()

            }
        }
        timer.start()
    }
}