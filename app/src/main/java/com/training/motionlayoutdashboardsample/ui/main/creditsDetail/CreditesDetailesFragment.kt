package com.training.motionlayoutdashboardsample.ui.main.creditsDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.transition.TransitionInflater
import com.training.motionlayoutdashboardsample.R

class CreditesDetailesFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_vcn_detail, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setSharedElementTransitionOnEnter()
        /*
         if you use "Glide"
         Well,the images you want to transition are loaded into the Fragment view by Glide.
         Loading images takes time, which means that the view runs its transitions before the image is available,
         messing up the animation. The trick here is to postpone the Fragment enter transition and resume it after the images
         finish loading.
        */
        postponeEnterTransition()
    }

    private fun setSharedElementTransitionOnEnter() {
        sharedElementEnterTransition = TransitionInflater.from(context)
            .inflateTransition(R.transition.shared_element_transition)
    }

}
