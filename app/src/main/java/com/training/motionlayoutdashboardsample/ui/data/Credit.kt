package com.training.motionlayoutdashboardsample.ui.data

data class Credit(val limit: String, val img: Int)
