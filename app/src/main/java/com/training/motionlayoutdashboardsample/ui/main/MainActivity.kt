package com.training.motionlayoutdashboardsample.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.training.motionlayoutdashboardsample.R

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navController by lazy { findNavController(R.id.fl_main_fragments_container) }
        var navHostFragment: NavHostFragment? =
            supportFragmentManager.findFragmentById(R.id.fl_main_fragments_container) as NavHostFragment?

    }


}