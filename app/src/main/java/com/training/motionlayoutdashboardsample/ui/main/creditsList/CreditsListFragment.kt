package com.training.motionlayoutdashboardsample.ui.main.creditsList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import com.training.motionlayoutdashboardsample.R
import com.training.motionlayoutdashboardsample.ui.adapter.CreditAdapter
import com.training.motionlayoutdashboardsample.ui.data.Credit
import kotlinx.android.synthetic.main.fragment_credits_list.view.*

class CreditsListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_credits_list, container, false)

        return view
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        setExitToFullScreenTransition()
//        setReturnFromFullScreenTransition()
//
//    }


//    private fun createAdapter() :CreditAdapter{
//
//    }


//    private fun setupRecyclerView(view: View, createAdapter: CreditAdapter) {
//        view.recycler_view_credits.run {
//            adapter = createAdapter
//
//            setHasFixedSize(true)
//
//            //1
//            postponeEnterTransition()
////2
//            viewTreeObserver.addOnPreDrawListener {
//                //3
//                startPostponedEnterTransition()
//                true
//            }
//
//        }
//
//    }

//
//    private fun createAdapter(): CreditAdapter {
//        return CreditAdapter(mNicolasCageMovies) {  ->
//            findNavController()
//                .navigate(action,
//                    FragmentNavigator.Extras.Builder()
//                        .addSharedElements(
//                            mapOf(posterView to posterView.transitionName,
//                                releaseDateView to releaseDateView.transitionName,
//                                ratingView to ratingView.transitionName)
//                        ).build()
//                )
//            navigate(toDoggoFragment, extraInfoForSharedElement)
//        }
//
//    private fun navigate(destination: NavDirections, extraInfo: FragmentNavigator.Extras) =
//        with(findNavController()) {
//            // 1
//            currentDestination?.getAction(destination.actionId)
//                ?.let {
//                    navigate(destination, extraInfo) //2 }
//                }
//
//        }
//
//    private fun setExitToFullScreenTransition() {
//        exitTransition =
//            TransitionInflater.from(context).inflateTransition(R.transition.credit_list_exit_transition)
//    }
//
//    private fun setReturnFromFullScreenTransition() {
//        reenterTransition =
//            TransitionInflater.from(context).inflateTransition(R.transition.credit_list_return_transition)
//    }
//
}

