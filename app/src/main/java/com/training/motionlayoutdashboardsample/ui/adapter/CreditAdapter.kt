package com.training.motionlayoutdashboardsample.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.training.motionlayoutdashboardsample.R
import com.training.motionlayoutdashboardsample.ui.data.Credit

class CreditAdapter(private val list: List<Credit>, private val cllback: () -> Unit) :
    RecyclerView.Adapter<CreditViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CreditViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CreditViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: CreditViewHolder, position: Int) {
        val credit: Credit = list[position]
        holder.itemView.setOnClickListener {
            cllback.invoke()
        }
        holder.bind(credit)
    }

    override fun getItemCount(): Int = list.size

}

class CreditViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_list_card_vcn, parent, false)) {
    private var imgCard: ImageView? = null
    private var txtLimit: TextView? = null


    init {
        imgCard = itemView.findViewById(R.id.iv_card)
        txtLimit = itemView.findViewById(R.id.tv_limit)
    }

    fun bind(credit: Credit) {
        imgCard?.setImageResource(credit.img)
        //transitionName = credit.img
        txtLimit?.text = credit.limit
    }

}